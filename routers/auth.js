const express = require('express');
const { check , validationResult } = require('express-validator');
const User = require('../models/User');
const bcryptJs = require('bcryptjs');
const passport = require('passport');
const router = express.Router();
const JWT_SECRET = 'moodAppSecretKey';
const errorr = 'Check+All+Field+Correctly+Filled+password+with+6+or+more+characters';

// router for user registration
router.post('/register-user',
 [
    check('name', 'Name is required').not().isEmpty(),
    check('email','Please include a valid email').isEmail(),
    check('password' ,'Please Enter a password with 6 or more characters').isLength({min:6}),
 ],
 async (req ,res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
       
       return res.redirect(`/register?msg=${errorr}`);
    }

    // destructure user input 
    const { name ,email ,password } = req.body;

    try{
        let user = await User.findOne({email});
        if(user){
            return res.redirect(`/register?msg=User+Is+Alreay+With+this+email${user.email}`);
        }
        user = await new User({
            name,
            email,
            password,
        });
        const salt = await bcryptJs.genSalt(10);
        user.password = await bcryptJs.hash(password ,salt);

        await user.save();
        return res.redirect('/login?msg=Account+Succesfully+Created+,Please+Login+to')
        
        
    }catch(error){
        return res.redirect(`/register?msg=${error}`);
    }

});

// login post method

router.post('/login',
[
    check('email','Please include a valid email').isEmail(),
    check('password' ,'Password is Required').exists(),
],
passport.authenticate('local',{
    failureRedirect: '/login',
    failureFlash: true,
},)
,
   async(req,res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.redirect(`/login?msg=${errorr}`);
    }
    return res.redirect('/?msg=Login+Successfully');
});

module.exports = router;
